package pl.poznan.iilo.adamkucz.year2023.dnd.characterclass;

import pl.poznan.iilo.adamkucz.year2023.dnd.item.Item;

import java.util.Optional;

public class PickPocketResult {
    public final Optional<Item> stolenItem;
    public final int type;

    public static final int SUCCESS = 0;
    public static final int FAILURE = 1;
    public static final int NOTICED = 2;


    public PickPocketResult(Item stolenItem) {
        this.type = SUCCESS;
        this.stolenItem = Optional.of(stolenItem);
    }
    public PickPocketResult(int type) {
        this.type = type;
        this.stolenItem = Optional.empty();
    }
}
