package pl.poznan.iilo.adamkucz.year2023.dnd.characterclass;

import pl.poznan.iilo.adamkucz.year2023.dnd.Alignment;
import pl.poznan.iilo.adamkucz.year2023.dnd.CharacterAbility;

public class Paladin extends Fighter {
    private static final String[] titles =  {"Gallant", "Keeper", "Protector",
            "Defender", "Warder", "Guardian", "Chevalier", "Justiciar", "Paladin"};

    private static final int[] maxExpLevels = {-1, 2_750, 5_500, 12_000, 24_000,
            45_000, 95_000, 175_000};

    private static final int[][] allowedSpells = {
    //levels 1  2  3  4
            {1, 0 ,0, 0},
            {2, 0, 0, 0},
            {2, 1, 0, 0},
            {2, 2, 0, 0},
            {2, 2, 1, 0},
            {3, 2, 1, 0},
            {3, 2, 1, 1},
            {3, 3, 1, 1},
            {3, 3, 2, 1},
            {3, 3, 3, 1},
            {3, 3, 3, 2},
            {3, 3, 3, 3}
    };

    public Paladin() {
        super("Paladin");
    }

    @Override
    public boolean isAllowed(Alignment alignment, CharacterAbility ability) {
        return !(ability.strength < 12) && ability.intelligence >= 9
                && ability.wisdom >= 13 && ability.constitution >= 9
                && ability.charisma >= 17
                && alignment.order.equals("lawful") && alignment.ethics.equals("good");
    }

    @Override
    public double expModifier(CharacterAbility ability) {
        return ability.strength > 15 && ability.wisdom > 15 ? 1.1 : 1;
    }

    @Override
    public int getMaxExpOfLevel(int level) {
        return level < maxExpLevels.length ? maxExpLevels[level] : (350_000 * (level + 1 - maxExpLevels.length));
    }

    @Override
    public String getTitle(int level) {
        return level < titles.length ? titles[level - 1] : (titles[titles.length - 1] + " (" + level + "th level)");
    }

    public int numberOfUsableSpells(int characterLevel, int clericalSpellLevel) {
        int magicLevel = characterLevel - 9;
        if (checkMagicArgs(magicLevel, clericalSpellLevel)) {
            return 0;
        }
        else if (magicLevel >= allowedSpells.length) {
            // final value shall be repeated after level 20
            return allowedSpells[allowedSpells.length - 1][clericalSpellLevel - 1];
        }
        else {
            return allowedSpells[magicLevel][clericalSpellLevel - 1];
        }
    }
    private boolean checkMagicArgs(int magicLevel, int clericalSpellLevel) {
        // checks if the provided arguments are incorrect
        return magicLevel < 0 || clericalSpellLevel > 4 || clericalSpellLevel < 1;
    }
    /* these are not implemented
    @Override
    public String[] knownActions() {return new String[]{"createfreehold", "layonhands", "curedisease", "detectevil"};}
    @Override
    public void applyInitialSpecialModifiers(DndCharacter character) {
        character.diseaseImmunity = true;
    }
     */
}


