package pl.poznan.iilo.adamkucz.year2023.dnd.characterclass;

import pl.poznan.iilo.adamkucz.year2023.dnd.Alignment;
import pl.poznan.iilo.adamkucz.year2023.dnd.CharacterAbility;
import pl.poznan.iilo.adamkucz.year2023.dnd.Dice;
import pl.poznan.iilo.adamkucz.year2023.dnd.DndCharacter;

public class Fighter extends CharacterClass {
    private static final String[] titles =  {"Veteran", "Warrior", "Swordsman",
            "Hero", "Swashbuckler", "Myrmidon", "Champion", "Superhero", "Lord"};

    private static final int[] maxExpLevels = {-1, 2_000, 4_000, 8_000, 18_000,
            35_000, 70_000, 125_000};

    public Fighter() {
        super("Fighter", 10, Integer.MAX_VALUE, "5d4");
    }
    protected Fighter(String name) {
        super(name, 10, Integer.MAX_VALUE, "5d4");
    }

    @Override
    public boolean isAllowed(Alignment alignment, CharacterAbility ability) {
        return ability.strength >= 9 && ability.constitution >= 7;
    }

    @Override
    public double expModifier(CharacterAbility ability) {
        return ability.strength > 15 ? 1.1 : 1;
    }

    @Override
    public int getMaxExpOfLevel(int level) {
        return level < maxExpLevels.length ? maxExpLevels[level] : 250_000 * (level + 1 - maxExpLevels.length) ;
    }

    @Override
    public String getTitle(int level) {
        return level < titles.length ? titles[level - 1] : (titles[titles.length - 1] + " (" + level + "th level)");
    }

    @Override
    public void levelUp(DndCharacter self) {
        self.increaseMaxHitPoints(self.getLevel() <= 9 ? Dice.roll(hitDieType) : 3);
    }

    /*@Override
    public String[] knownActions() {return new String[]{"createfreehold"};} - not implemented

     */
}

