package pl.poznan.iilo.adamkucz.year2023.dnd.characterclass;

import pl.poznan.iilo.adamkucz.year2023.dnd.Alignment;
import pl.poznan.iilo.adamkucz.year2023.dnd.CharacterAbility;
import pl.poznan.iilo.adamkucz.year2023.dnd.Dice;
import pl.poznan.iilo.adamkucz.year2023.dnd.DndCharacter;

public class Cleric extends CharacterClass {
    public Cleric() {
        super("Cleric", 8, Integer.MAX_VALUE, "3d6");
    }

    private static final String[] titles = {"Acolyte", "Adept", "Priest", "Curate",
            "Canon", "Lama", "Patriarch", "High Priest"};

    private static final int[] maxExpLevels = {-1, 1_500, 3_000, 6_000, 13_000, 27_500, 55_000, 110_000};

    // The indexes of the 2d array are for the levels,
    // while the indexes of the inner arrays show the spell levels we are interested in.
    // The 17 or 18 indicate that they are reserved for characters of that wisdom
    private static final int[][] allowedSpells = {
            {1, 0, 0, 0, 0, 0, 0},
            {2, 0, 0, 0, 0, 0, 0},
            {2, 1, 0, 0, 0, 0, 0},
            {3, 2, 0, 0, 0, 0, 0},
            {3, 3, 1, 0, 0, 0, 0},
            {3, 3, 2, 0, 0, 0, 0},
            {3, 3, 2, 1, 0, 0, 0},
            {3, 3, 3, 2, 0, 0, 0},
            {4, 4, 3, 2, 1, 0, 0},
            {4, 4, 3, 3, 2, 0, 0},
            {5, 4, 4, 3, 2, 0, 0},
            {6, 5, 5, 3, 2, 2, 0},
            {6, 6, 6, 4, 2, 2, 0},
            {7, 7, 7, 5, 4, 2, 0},
            {7, 7, 7, 6, 5, 3, 0},
            {8, 8, 8, 6, 5, 3, 1},
            {8, 8, 8, 7, 6, 4, 1},
            {9, 9, 9, 7, 6, 4, 2},
            {9, 9, 9, 8, 7, 5, 2},
            {9, 9, 9, 9, 8, 6, 2},
            {9, 9, 9, 9, 9, 6, 3},
            {9, 9, 9, 9, 9, 7, 3},
            {9, 9, 9, 9, 9, 8, 3},
            {9, 9, 9, 9, 9, 8, 4},
            {9, 9, 9, 9, 9, 9, 4},
            {9, 9, 9, 9, 9, 9, 5},
            {9, 9, 9, 9, 9, 9, 6},
            {9, 9, 9, 9, 9, 9, 7}
    };

    // TODO implement the check for having multiple classes and a different race
    @Override
    public boolean isAllowed(Alignment alignment, CharacterAbility ability) {
        return ability.wisdom >= 9 && !(alignment.order.equals("neutral") && alignment.ethics.equals("neutral"));
    }

    @Override
    public double expModifier(CharacterAbility ability) {
        return ability.wisdom > 15 ? 1.1 : 1;
    }

    @Override
    public int getMaxExpOfLevel(int level) {
        return level < maxExpLevels.length ? maxExpLevels[level] : (225_000 * (level + 1 - maxExpLevels.length));
    }

    @Override
    public String getTitle(int level) {
        return level < titles.length ? titles[level - 1] : (titles[titles.length - 1] + " (" + level + "th level)");
    }

    @Override
    public void levelUp(DndCharacter self) {
        self.increaseMaxHitPoints(self.getLevel() <= 9 ? Dice.roll(hitDieType) : 2);
    }

    public int numberOfUsableSpells(DndCharacter self, int clericalSpellLevel) {
        int returnValue;
        if (self.getLevel() >= allowedSpells.length) {
            returnValue = allowedSpells[allowedSpells.length - 1][clericalSpellLevel - 1];
        } else {
            returnValue = allowedSpells[self.getLevel() - 1][clericalSpellLevel - 1];
        }
        if ((self.getLevel() == 11 && self.getAbility().wisdom >= 17 && clericalSpellLevel == 6) ||
                self.getLevel() == 16 && self.getAbility().wisdom >= 18 && clericalSpellLevel ==7)  {
            // a 'beautified' version of the special cases included in the table
            returnValue = 1;
        }
        return checkForIntelligence(self, clericalSpellLevel) ? returnValue : 0;
    }

    private boolean checkForIntelligence(DndCharacter self, int spellLevel) {
        return spellLevel < 5 || self.getAbility().intelligence * 2 >= spellLevel;
    }

    // TODO all clerical spells
}
