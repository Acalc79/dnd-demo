package pl.poznan.iilo.adamkucz.year2023.dnd.world;

import pl.poznan.iilo.adamkucz.year2023.dnd.item.Item;
import pl.poznan.iilo.adamkucz.year2023.dnd.util.Util;

public class Container extends Item {
    public Item[] contents;

    public Container(String identifier) {
        super(identifier);
    }

    public boolean contains(Item item) {
        return Util.hasElement(contents, item);
    }
}
