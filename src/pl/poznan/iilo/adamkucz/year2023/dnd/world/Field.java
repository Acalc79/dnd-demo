package pl.poznan.iilo.adamkucz.year2023.dnd.world;

public class Field extends Location {
    private int xSize;
    private int ySize;

    public Field(Coordinates coords) {
        super(coords);
    }

    public Location at(double xDiff, double yDiff) {
        if (Math.abs(xDiff) > xSize/2 || Math.abs(yDiff) > ySize/2) {
            throw new IllegalArgumentException("Given location outside the field");
        }
        return new FieldLocation(xDiff, yDiff);
    }

    private class FieldLocation extends Location {
        public FieldLocation(double xDiff, double yDiff) {
            super(Field.this.coords().add(new Coordinates(xDiff, yDiff, 0)));
        }
    }
}
