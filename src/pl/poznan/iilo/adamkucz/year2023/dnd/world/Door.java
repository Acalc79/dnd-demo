package pl.poznan.iilo.adamkucz.year2023.dnd.world;
public class Door extends Barrier {
    public Door(Location side1, Location side2) {
        super(side1, side2);
    }
}
