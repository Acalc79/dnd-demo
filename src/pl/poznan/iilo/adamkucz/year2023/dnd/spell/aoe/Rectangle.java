package pl.poznan.iilo.adamkucz.year2023.dnd.spell.aoe;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Distance;

public class Rectangle extends AreaOfEffect {
    public final Distance sideA, sideB;

    public Rectangle(Distance sideA, Distance sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }
}
