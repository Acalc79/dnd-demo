package pl.poznan.iilo.adamkucz.year2023.dnd.spell;

public enum SavingThrow {
    NONE, HALF, NEGATE;
}
