package pl.poznan.iilo.adamkucz.year2023.dnd.spell.aoe;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Distance;

public class Sector extends AreaOfEffect {
    public final Distance radius;
    public final int angle;

    public Sector(Distance radius, int angle) {
        this.radius = radius;
        this.angle = angle;
    }

    public Sector(int radius, int angle) {
        this.radius = Distance.ofIndoor(radius);
        this.angle = angle;
    }
}
