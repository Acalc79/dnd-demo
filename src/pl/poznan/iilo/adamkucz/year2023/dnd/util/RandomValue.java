package pl.poznan.iilo.adamkucz.year2023.dnd.util;

import pl.poznan.iilo.adamkucz.year2023.dnd.Dice;
import java.util.Arrays;
import java.util.OptionalInt;

public class RandomValue {
    private final int diceType;
    private final int diceNumber;
    private final int additiveModifier;

    public RandomValue(int diceType, int diceNumber, int additiveModifier) {
        this.diceType = diceType;
        this.diceNumber = diceNumber;
        this.additiveModifier = additiveModifier;
    }

    public RandomValue(int minimum, int maximum) {
        int modifier = 0;
        int[] diceTypes = {4, 6, 8, 10, 12, 20};

        OptionalInt type = createStream(maximum, minimum, diceTypes);

        if (!type.isPresent()) {
            modifier = findDifference(maximum, minimum, diceTypes);
            // we get rid of the difference and get the final dice type
            maximum -= modifier;
            minimum -= modifier;
            type = createStream(maximum, minimum, diceTypes);
        }

        additiveModifier = modifier;
        diceType = type.getAsInt();
        diceNumber = minimum;
    }

    private OptionalInt createStream(int maximum, int minimum, int[] diceTypes) {
        // this stream filters out dice types that do not fit the bounds and then chooses the smallest one available
        return Arrays.stream(diceTypes)
                .filter(num -> checkBounds(maximum, minimum, num))
                .findFirst();
    }
    private int findDifference(int maximum, int minimum, int[] diceTypes) {
        int difference = 0;
        // here we loop until we find the correct dice type while counting the difference (the additive multiplier)
        while (!createStream(maximum - difference, minimum - difference, diceTypes).isPresent()) {
            int newMinimum = minimum - difference;
            difference++;
            if (newMinimum == 0) {
                throw new IllegalArgumentException("Incorrect arguments for the bounds");
            }
        }
        return difference;
    }
    private boolean checkBounds(int max, int min, int type) {
        // here we check if the maximum is a multiple of the given dice type and if it will actually give us the correct dice roll
        return max % type == 0 && max / type == min;
    }

    public static int between(int minimum, int maximum) {
        return new RandomValue(minimum, maximum).roll();
    }

    public int roll() {
        return Dice.roll(diceNumber, diceType) + additiveModifier;
    }

    public int minimum() {
        return diceNumber + additiveModifier;
    }

    public int maximum() {
        return diceNumber * diceType + additiveModifier;
    }
}
