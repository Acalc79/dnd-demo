package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.item.Book;

public class Read extends TargetedAction<Book> {

    public static final String NAME = "read";

    public Read(String line) {
        super(line);
    }

    public Read(Book target) {
        super(NAME, target);
    }
}
