package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Location;

public class FindTraps extends TargetedAction<Location> {
    public static final String NAME = "findtraps";

    public FindTraps(String line) {
        super(line);
    }

    public FindTraps(Location target) {
        super(NAME, target);
    }
}
