package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Fire;

public class IncreaseFire extends TargetedAction<Fire> {

    public IncreaseFire(String line) {
        super(line);
    }

    public IncreaseFire(Fire target) {
        super("createfire", target);
    }
}