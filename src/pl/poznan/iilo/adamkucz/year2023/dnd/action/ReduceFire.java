package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Fire;

public class ReduceFire extends TargetedAction<Fire> {

    public ReduceFire(String line) {
        super(line);
    }

    public ReduceFire(Fire target) {
        super("reducefire", target);
    }
}