package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Door;

public class ListenAtDoor extends TargetedAction<Door> {
    public static final String NAME = "listenatdoor";

    public ListenAtDoor(String line) {
        super(line);
    }

    public ListenAtDoor(Door target) {
        super(NAME, target);
    }
}
