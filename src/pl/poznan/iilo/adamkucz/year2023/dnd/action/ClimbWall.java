package pl.poznan.iilo.adamkucz.year2023.dnd.action;

import pl.poznan.iilo.adamkucz.year2023.dnd.world.Wall;

public class ClimbWall extends TargetedAction<Wall> {
    public static final String NAME = "climbwall";

    public ClimbWall(String line) {
        super(line);
    }

    public ClimbWall(Wall target) {
        super(NAME, target);
    }
}
