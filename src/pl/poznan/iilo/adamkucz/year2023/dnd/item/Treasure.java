package pl.poznan.iilo.adamkucz.year2023.dnd.item;

import pl.poznan.iilo.adamkucz.year2023.dnd.Dice;
import pl.poznan.iilo.adamkucz.year2023.dnd.util.RandomValue;

import java.util.ArrayList;
import java.util.List;

public class Treasure {
    // treasures types are single capital letters (A-Z)
    public static List<Item> generate(char type) {
        switch(type) {
            case 'A':
                List<Item> items = new ArrayList<>();
                if (Dice.rollForChance(0.25)) {
                    items.add(new Money(RandomValue.between(1, 6) * 1000, Money.CoinType.COPPER));
                }
                if (Dice.rollForChance(0.3)) {
                    items.add(new Money(RandomValue.between(1, 6) * 1000, Money.CoinType.SILVER));
                }
                if (Dice.rollForChance(0.35)) {
                    items.add(new Money(RandomValue.between(1, 6) * 1000, Money.CoinType.ELECTRUM));
                }
                if (Dice.rollForChance(0.40)) {
                    items.add(new Money(RandomValue.between(1, 10) * 1000, Money.CoinType.GOLD));
                }
                if (Dice.rollForChance(0.25)) {
                    items.add(new Money(RandomValue.between(1, 4) * 100, Money.CoinType.PLATINUM));
                }
                /*
                if (Dice.rollForChance(0.6)) {
                    // functional (advanced)
                    // items.addAll(Stream.generate(() -> Gem.random()).limit(RandomValue.between(4, 40))
                    //        .collect(Collectors.toCollection()));
                    int numberOfGems = RandomValue.between(4, 40);
                    for (int i = 0; i < numberOfGems; i++) {
                        // TODO: implement gems, including random creation
                        //  items.add(Gem.random());
                    }
                }
                if (Dice.rollForChance(0.5)) {
                    // functional (advanced)
                    // items.addAll(Stream.generate(() -> Jewelry.random()).limit(RandomValue.between(4, 40))
                    //        .collect(Collectors.toCollection()));
                    int numberOfGems = RandomValue.between(3, 30);
                    for (int i = 0; i < numberOfGems; i++) {
                        // TODO: implement jewelry, including random creation
                        //  items.add(Jewelry.random());
                    }
                }
                // TODO: add chance for 3 maps or magic and a sword/armor/weapon
                */
                return items;
        }
        throw new IllegalArgumentException("Valid treasure types are A-Z, got: " + type);
    }
}
