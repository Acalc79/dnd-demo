package pl.poznan.iilo.adamkucz.year2023.dnd.item;

public class SpellBook extends Book {

    public SpellBook(String title, String language) {
        super(title, language);
    }
}
