package pl.poznan.iilo.adamkucz.year2023.dnd;

import pl.poznan.iilo.adamkucz.year2023.dnd.action.Action;
import pl.poznan.iilo.adamkucz.year2023.dnd.item.Item;
import pl.poznan.iilo.adamkucz.year2023.dnd.spell.Spell;
import pl.poznan.iilo.adamkucz.year2023.dnd.util.Util;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.Location;
import pl.poznan.iilo.adamkucz.year2023.dnd.item.Money;
import pl.poznan.iilo.adamkucz.year2023.dnd.world.WorldElement;

import java.util.Arrays;
import java.util.Optional;

public class DndBeing extends WorldElement {
    protected String name;
    protected int hitPoints;
    protected int maxHitPoints;
    protected String[] knownLanguages;
    protected Item[] inventory;
    protected Money money; // everything is expressible as copper pieces for now
    private Location currentLocation;
    private Spell[] activeSpells;

    public DndBeing(String name, int maxHitPoints) {
        this.name = name;
        this.maxHitPoints = maxHitPoints;
        this.knownLanguages = new String[]{"common tongue"};
        inventory = new Item[]{};
        money = Money.none();
        hitPoints = maxHitPoints;
        activeSpells = new Spell[]{};
    }

    // copy constructor, does a shallow copy of activeSpells
    public DndBeing(DndBeing other) {
        this(other.name, other.maxHitPoints);
        this.knownLanguages = Arrays.copyOf(other.knownLanguages, other.knownLanguages.length);
        // functional (advanced)
        // copy.inventory = Arrays.stream(this.inventory).map(Item::copy).toArray(Item[]::new);
        this.inventory = new Item[other.inventory.length];
        for (int i = 0; i < other.inventory.length; i++) {
            this.inventory[i] = other.inventory[i].copy();
        }
        this.money = other.money;
        this.hitPoints = other.hitPoints;
        this.activeSpells = Arrays.copyOf(other.activeSpells, other.activeSpells.length);
    }

    public boolean knowsLanguage(String language) {
        return Util.hasElement(knownLanguages, language);
    }

    public boolean hasItem(Item item) {
        return Util.hasElement(inventory, item);
    }

    public Item[] allItems() {
        return inventory;
    }

    public void increaseMaxHitPoints(int extraHitPoints) {
        maxHitPoints += extraHitPoints;
    }

    public String getName() {
        return name;
    }

    public Optional<Item> getItem(String identifier) {
        for (int i = 0; i < inventory.length; i++) {
            if (inventory[i].getIdentifier().equals(identifier)) {
                return Optional.of(inventory[i]);
            }
        }
        return Optional.empty();
    }

    public void addToInventory(Item item) {
        inventory = Util.extendArray(inventory, item);
    }

    public boolean removeFromInventory(Item item) {
        if (Util.hasElement(inventory, item)) {
            inventory = Util.removeFromArray(inventory, item);
            return true;
        }
        return false;
    }

    public void doAction(Action action) {
        for (int i = 0; i < activeSpells.length; i++) {
            if (activeSpells[i].doAction(this, action)) {
                return;
            }
        }
    }

    public void moveTo(Location destination) {
        currentLocation = destination;
    }

    public Location getCurrentLocation() {
        return currentLocation;
    }
}
